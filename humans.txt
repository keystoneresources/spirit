/* the humans responsible */
/* humanstxt.org */


/* TEAM */
  Web Director: Roger Sullivan
  Developer: Shonna Goddoy
  Site: http://keystoneresources.com/
  Twitter: @kr_creative
  Location: Houston, TX
  

  # TECHNOLOGY COLOPHON
    HTML5, CSS3, H5BP, Twitter Bootstrap
    jQuery, Angular, Modernizr
    Coda