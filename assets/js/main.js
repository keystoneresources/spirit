$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      hoverUnbind();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      hoverUnbind();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      hoverUnbind();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerEnable();
	      hoverUnbind();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
		    slideDrawerDisable();
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      hoverInit();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      hoverInit();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
	if($('body.type-5').length){
		var $children = $('.awards-wrapper span');
    for(var i = 0, l = $children.length; i < l; i += 3) {
      $children.slice(i, i+3).wrapAll('<div></div>'); 
    } 
    
    var $childrens = $('.job-posts-container .job-post');
    for(var i = 0, l = $childrens.length; i < l; i += 2) {
      $childrens.slice(i, i+2).wrapAll('<div class="row"><div class="col-xs-12"></div></div>'); 
    } 
    
		$('.awards-wrapper .rotator').cycle({
      speed: 1300,
      timeout: 0,
      fx: 'fade',
      width: '100%', 
      fit: 1,
      slides: '> div',
      prev: '.awards-wrapper .arrow.prev',
      next: '.awards-wrapper .arrow.next'
    }); 
    if($('.awards-wrapper .rotator').find('img').length < 4){
      $('.awards-wrapper .arrow').hide();
    }
	}//if
	 
	if($('body.type-5').length || $('body.type-6').length || $('body.type-8').length){
		$('.popup-trigger').each(function(){
	    $(this).click(function(e){
		    e.preventDefault(); 
				$(this).next('.popup-container').show().animate({
					width: '100%', 
					height: '100%',
					top: '0px',
					marginLeft: '-50%',
					opacity: 1
				}, 200, function(){ 
					$(this).find('.popup').show();
					$('body').addClass('popup-open');
				});
	    });
    });
    $('.popup-close').each(function(){
	    $(this).click(function(e){
				e.preventDefault();
				$('.popup').hide();
				$('body').removeClass('popup-open'); 
				$(this).closest('.popup-container').animate({
					width: '0%', 
					height: '0%',
					top: '50%',
					marginLeft: '-10%',
					opacity: 0
				}, 200, function(){
					$('.popup-container').hide(); 
				});
			});
    });
	}//if
	
	if($('body.type-6').length){
		var $children = $('.team .team-member');
    for(var i = 0, l = $children.length; i < l; i += 3) {
      $children.slice(i, i+3).wrapAll('<div class="team-member-row"></div>'); 
    }
    if($('.team-member-row:last .team-member').length == 1){
	    $('.team-member-row:last .team-member').addClass('team-single-row');
    }else if($('.team-member-row:last .team-member').length == 2){
	    $('.team-member-row:last .team-member').addClass('team-double-row');
    }
	}
	
	
	if($('body.type-8 form .error').length){
		$('.popup-container').addClass("error-open");
	}
	
	if(!Modernizr.input.placeholder){
	  $('#contact-form label').show();
	}
	

	$('object.svg-object').hide(); 
	$('object.svg-object').each(function(index){
		$(this).parent().find('svg').attr("id",'svg'+index);
		var svgId = '#svg'+index;
		var svgImage = '..'+$(this).attr("data");
		Snap.load(svgImage, svgLoaded);
		function svgLoaded(data){
			Snap(svgId).append(data);
		}
	});
	  

  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }

  function hoverInit(){
		//content block hover
		if($(".content-block").length){
			$(".content-block a.button").hover(function(){
		    $(this).parent().addClass("active");
		  },function(){
		    $(this).parent().removeClass("active");
		  });
		}

		// hover state for events
		if($(".small-image").length){
			$(".small-image").hover(function(){
		    $(this).find(".overlay-info").show();
		  },function(){
		    $(this).find(".overlay-info").hide();
		  });
		}
  }

	function hoverUnbind(){
		//content block unbind
		$(".content-block a.button").unbind();
		//events unbind
		$(".small-image").unbind();
  }

	// find and use backstretch if present
	if($("#backstretch-src").length){
		var backSrc = $("#backstretch-src").attr('src');
		$(".backstretch-target").backstretch(backSrc);
	}

}); //End Document Ready